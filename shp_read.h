#ifndef _SHP_READ_H
#define _SHP_READ_H

#include <string>
#include <vector>
#include "shapefil.h"

class ShpObject
{
public:
	ShpObject();
	ShpObject(const ShpObject& p);
	virtual ~ShpObject();

	ShpObject& operator=(const ShpObject& other);

	void Clear();
	const char *TypeName();
	const char *PartTypeName(int i);

	int shpType;
	std::vector<int> partTypes;
	std::vector<std::vector<std::vector<double> > > decodedParts;
};

class ShpRead 
{
public:
	ShpRead();
	virtual ~ShpRead();

	int Open(const std::string &fina);
	void Close();

	void Get(int i);
	void Dump();

	//Callbacks
	void (*foundBounds)(int *userdata, int index, int shpType, 
		double xMin, double yMin, double zMin, 
		double xMax, double yMax, double zMax);

	void (*foundObject)(int *userdata, int index, class ShpObject &out);
	int *userdata;

	int nEntities, nShapeType;

private:
	SHPHandle h;
	class ShpObject tmpObject;
};

#endif //_SHP_READ_H

