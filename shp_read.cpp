
#include <iostream>
#include <vector>
#include "shp_read.h"
using namespace std;

ShpObject::ShpObject()
{
	shpType = -1;
}

ShpObject::ShpObject(const ShpObject& p)
{
	*this = p;
}

ShpObject::~ShpObject()
{

}

ShpObject& ShpObject::operator=(const ShpObject& obj)
{
	shpType = obj.shpType;
	partTypes = obj.partTypes;
	decodedParts = obj.decodedParts;

	return *this;
}

void ShpObject::Clear()
{
	shpType = -1;
	partTypes.clear();
	decodedParts.clear();
}

const char *ShpObject::TypeName()
{
	return SHPTypeName(this->shpType);
}

const char *ShpObject::PartTypeName(int i)
{
	return SHPPartTypeName(this->partTypes[i]);
}

// ********************

ShpRead::ShpRead()
{
	nEntities=0; nShapeType=0;
	this->h = nullptr;
	foundBounds = nullptr;
	foundObject = nullptr;
	userdata = nullptr;
}

ShpRead::~ShpRead()
{
	this->Close();
}

int ShpRead::Open(const std::string &fina)
{
	if(h!=nullptr)
		throw runtime_error("Shapefile already open");

	this->h = SHPOpen(fina.c_str(), "rb");
	if(h==nullptr)
	{
		cout << "Failed to open shp" << endl;
		return -1;
	}

	SHPGetInfo(h, &this->nEntities, &this->nShapeType, nullptr, nullptr);

	return 0;
}

void ShpRead::Close()
{
	SHPClose(h);
}

void ShpRead::Get(int i)
{
	if(h==nullptr)
		throw runtime_error("Shapefile not open");

	SHPObject *obj = SHPReadObject(h, i);
	if(obj == nullptr)
		throw runtime_error("Error doing SHPReadObject");

	int &shpType = obj->nSHPType; //Shape Type (SHPT_* - see list above)
	int &shapeId = obj->nShapeId; //Shape Number (-1 is unknown/unassigned)

	int &parts = obj->nParts; //# of Parts (0 implies single part with no info)
	int *panPartStart = obj->panPartStart;  //Start Vertex of part
	int *panPartType = obj->panPartType;   //Part Type (SHPP_RING if not SHPT_MULTIPATCH)
	int &vertices = obj->nVertices; //Vertex list 

	if(foundBounds != nullptr)
	{
		double &xMin = obj->dfXMin; //Bounds in X, Y, Z and M dimensions
		double &yMin = obj->dfYMin;
		double &zMin = obj->dfZMin;

		double &xMax = obj->dfXMax;
		double &yMax = obj->dfYMax;
		double &zMax = obj->dfZMax;
		foundBounds(this->userdata, shapeId, shpType, xMin, yMin, zMin, xMax, yMax, zMax);
	}

	tmpObject.Clear();
	tmpObject.shpType = shpType;
	std::vector<int> partStarts, partTypes;
	for(int k=0; k<parts; k++)
	{
		partStarts.push_back(panPartStart[k]);
		partTypes.push_back(panPartType[k]);
	}
	tmpObject.partTypes = partTypes;

	if(foundObject != nullptr)
	{
		int part = -1;

		for(int j=0; j<vertices; j++)
		{
			if((part+1) < (int)partStarts.size() and partStarts[part+1] == j)
				part ++;

			std::vector<double> v = {obj->padfX[j], obj->padfY[j], obj->padfZ[j]};

			if(part != -1)
			{
				if(part >= (int)tmpObject.decodedParts.size())
					tmpObject.decodedParts.resize(part+1);
				tmpObject.decodedParts[part].push_back(v);
			}
			else
			{
				//When parts are not specified, assign to first part
				if(tmpObject.decodedParts.size() < 1)
					tmpObject.decodedParts.resize(1);
				if(tmpObject.partTypes.size() < 1)
					tmpObject.partTypes.resize(1);
				tmpObject.decodedParts[0].push_back(v);
				tmpObject.partTypes[0] = -1;
			}
		}
		
		if(tmpObject.partTypes.size() != tmpObject.decodedParts.size())
			throw runtime_error("Error in decoded object");
		foundObject(this->userdata, shapeId, tmpObject);
	}

	SHPDestroyObject(obj);
}

void ShpRead::Dump()
{
	if(h==nullptr)
		throw runtime_error("Shapefile not open");

	for(int i=0; i<this->nEntities; i++)
		this->Get(i);
}

