#include "dbindex.h"
#include <iostream>
using namespace std;

int main()
{
	class DbIndex dbIndex;
	dbIndex.Open("test.db");

	dbIndex.Clear();

	dbIndex.AddBbox(0, -80.7749, -80.7747, 35.3776, 35.3778);
	dbIndex.AddBbox(1, -80.7749, -80.7747, 35.3776, 35.3778);
	dbIndex.AddBbox(7, -80.7749, -80.7747, 35.3776, 35.3778);
	dbIndex.AddBbox(14, 43.2465, 43.2465, -21.353, -21.353);

	std::vector<int> result;
	dbIndex.Query(-81.08, -80.58, 35.00, 35.44, true, result);
	for(size_t i=0; i<result.size(); i++)
		cout << result[i] << endl;
}


