
all: shp_dump dbexample shp_index

shp_dump: shp_dump.cpp shp_read.cpp
	g++ -Wall shp_dump.cpp shp_read.cpp -lshp -o shp_dump

dbexample: dbexample.cpp dbindex.cpp
	g++ -Wall dbexample.cpp dbindex.cpp -lsqlite3 -o dbexample

shp_index: shp_index.cpp dbindex.cpp shp_read.cpp
	g++ -Wall shp_index.cpp dbindex.cpp shp_read.cpp -lshp -lsqlite3 -o shp_index

