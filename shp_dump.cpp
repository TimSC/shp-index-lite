#include "shp_read.h"
#include <iostream>
using namespace std;

void PrintBbox(int *userdata, int index, int shpType, 
		double xMin, double yMin, double zMin, 
		double xMax, double yMax, double zMax)
{
	cout << index << "," << SHPTypeName(shpType) << endl;
	cout << "bbox " << xMin << "," << yMin << "," << zMin << "," 
		<< xMax << "," << yMax << "," << zMax << endl; 
}

void PrintObject(int *userdata, int index, class ShpObject &out)
{
	cout << index << "," << out.TypeName() << endl;

	for(size_t i=0; i<out.decodedParts.size(); i++)
	{
		cout << i << "," << out.PartTypeName(i);
		std::vector<std::vector<double> > &part = out.decodedParts[i];
		for(size_t j=0; j<part.size(); j++)
		{
			std::vector<double> &vertex = part[j];
			cout << " (";
			for(size_t k=0; k<vertex.size(); k++)
			{
				cout << vertex[k] << ",";
			}
			cout << ")";
		}
		cout << endl;
	}
}

int main(int argc, const char **argv)
{
	class ShpRead shpRead;
	shpRead.foundBounds = &PrintBbox;
	shpRead.foundObject = &PrintObject;
	//shpRead.Open("/home/tim/Desktop/shp/stations/stations.shp");
	shpRead.Open("/home/tim/Desktop/shp/Community_Forests/Community_Forests.shp");
	cout << "Found " << shpRead.nEntities << " entities" << endl;
	shpRead.Dump();

	return 0;
}

