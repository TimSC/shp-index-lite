#include "dbindex.h"
#include "shp_read.h"
#include <iostream>
using namespace std;

void StoreBbox(int *userdata, int index, int shpType, 
		double xMin, double yMin, double zMin, 
		double xMax, double yMax, double zMax)
{
	class DbIndex &dbIndex = *(class DbIndex *)userdata;

	//cout << index << "," << SHPTypeName(shpType) << endl;
	//cout << "bbox " << xMin << "," << yMin << "," << zMin << "," 
	//	<< xMax << "," << yMax << "," << zMax << endl; 

	dbIndex.AddBbox(index, xMin, xMax, yMin, yMax);
}

int main(int argc, char **argv)
{
	string inputShp = "map.shp";
	string outputDb = "test.db";

	if(argc >= 2)
		inputShp = argv[1];
	if(argc >= 3)
		outputDb = argv[2];

	class DbIndex dbIndex;
	dbIndex.Open(outputDb);
	dbIndex.Clear();
	dbIndex.BeginTransaction();

	class ShpRead shpRead;
	shpRead.foundBounds = &StoreBbox;
	shpRead.userdata = (int *)&dbIndex;
	shpRead.Open(inputShp);
	cout << "Found " << shpRead.nEntities << " entities" << endl;
	shpRead.Dump();

	dbIndex.CommitTransaction();

	/*std::vector<int> result;
	dbIndex.Query(-81.08, -80.58, 35.00, 35.44, true, result);
	for(size_t i=0; i<result.size(); i++)
		cout << result[i] << endl;*/

	cout << "All done!" << endl;
	return 0;
}

