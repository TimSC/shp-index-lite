#ifndef _DB_INDEX_H
#define _DB_INDEX_H

#include <string>
#include <map>
#include <vector>
#include <sqlite3.h>

class DbIndex
{
public:

	DbIndex();
	virtual ~DbIndex();

	int Open(const std::string &fina);
	int Clear();
	int AddBbox(int id, double xMin, double xMax, double yMin, double yMax);

	int BeginTransaction();
	int CommitTransaction();

	int Query(double xMin, double xMax, double yMin, double yMax, bool overlapping, std::vector<int> &out);
	
private:
	sqlite3 *pDb;
	std::map<int, sqlite3_stmt *> insertStmts;

	sqlite3_stmt *PrepareBboxStatement(int insertSize);
	int Exec(const std::string &sql);
};

#endif //_DB_INDEX_H

