
#include <string>
#include <iostream>
#include <sqlite3.h>
#include "dbindex.h"
using namespace std;

DbIndex::DbIndex()
{
	pDb=nullptr;
}

DbIndex::~DbIndex()
{
	for(auto it=insertStmts.begin(); it!=insertStmts.end(); it++)
	{
		int ret = sqlite3_finalize(it->second);
		if(ret != SQLITE_OK) {cout << "sqlite3_finalize " << ret << " " << sqlite3_errmsg(pDb) << endl;}
		it->second = nullptr;
	}
	insertStmts.clear();

	sqlite3_close(pDb);
}

int DbIndex::Open(const std::string &fina)
{
	if(pDb != nullptr)
		return -1;

	int ret = sqlite3_open(fina.c_str(), &pDb);
	if(ret != SQLITE_OK)
	{
		cout << "sqlite3_open " << ret << " " << sqlite3_errmsg(pDb) << endl;
		return ret;
	}

	//Initialize schema
	string sql = "CREATE VIRTUAL TABLE IF NOT EXISTS shp_index USING rtree(id, minX, maxX, minY, maxY);";
	return this->Exec(sql);
}

int DbIndex::Clear()
{
	if(pDb == nullptr)
		return -1;

	string sql = "DELETE FROM shp_index;";
	return this->Exec(sql);
}

sqlite3_stmt *DbIndex::PrepareBboxStatement(int insertSize)
{
	string sql = "INSERT INTO shp_index VALUES";
	for(int i=0; i<insertSize; i++)
	{
		if(i>=1) sql += ",";
		sql += " (?,?,?,?,?)";
	}
	sql += ";";
	sqlite3_stmt *pStmt =nullptr;
	int ret = sqlite3_prepare_v2(pDb, sql.c_str(), -1, &pStmt, nullptr);
	if(ret != SQLITE_OK)
		cout << ret << " " << sqlite3_errmsg(pDb) << endl;
	return pStmt;
}

int DbIndex::AddBbox(int id, double xMin, double xMax, double yMin, double yMax)
{
	if(pDb == nullptr)
		return -1;
	std::map<int, sqlite3_stmt *>::iterator it = insertStmts.find(1);
	sqlite3_stmt *pStmt = nullptr;
	if(it == insertStmts.end())
	{
		pStmt = PrepareBboxStatement(1);
		insertStmts[1] = pStmt;
	}
	else
		pStmt = it->second;
	if(pStmt == nullptr)
		return -2;
	
	int ret = sqlite3_bind_int(pStmt, 1, id);
	if(ret != SQLITE_OK) {cout << ret << " " << sqlite3_errmsg(pDb) << endl; return ret;}
	ret = sqlite3_bind_double(pStmt, 2, xMin);
	if(ret != SQLITE_OK) {cout << ret << " " << sqlite3_errmsg(pDb) << endl; return ret;}
	ret = sqlite3_bind_double(pStmt, 3, xMax);
	if(ret != SQLITE_OK) {cout << ret << " " << sqlite3_errmsg(pDb) << endl; return ret;}
	ret = sqlite3_bind_double(pStmt, 4, yMin);
	if(ret != SQLITE_OK) {cout << ret << " " << sqlite3_errmsg(pDb) << endl; return ret;}
	ret = sqlite3_bind_double(pStmt, 5, yMax);
	if(ret != SQLITE_OK) {cout << ret << " " << sqlite3_errmsg(pDb) << endl; return ret;}

	ret = sqlite3_step(pStmt);
	if(ret != SQLITE_DONE) {cout << "sqlite3_step " << ret << " " << sqlite3_errmsg(pDb) << endl;  return ret;}

	ret = sqlite3_reset(pStmt);
	if(ret != SQLITE_OK) {cout << ret << " " << sqlite3_errmsg(pDb) << endl; return ret;}
	return ret;
}

int DbIndex::BeginTransaction()
{
	if(pDb == nullptr)
		return -1;

	string sql = "BEGIN TRANSACTION;";
	return this->Exec(sql);

}

int DbIndex::CommitTransaction()
{
	if(pDb == nullptr)
		return -1;

	string sql = "COMMIT TRANSACTION;";
	return this->Exec(sql);
}

int DbIndex::Exec(const std::string &sql)
{
	char *errMsg = nullptr;
	int ret = sqlite3_exec(pDb, sql.c_str(), nullptr, nullptr, &errMsg);	
	if(ret != SQLITE_OK)
	{
		cout << ret << " ";
		if (errMsg!=nullptr) cout << errMsg << endl;
		cout << endl;
	}
	sqlite3_free(errMsg);
	return ret;
}

int DbIndex::Query(double xMin, double xMax, double yMin, double yMax, bool overlapping, std::vector<int> &out)
{
	if(pDb == nullptr)
		return -1;

	string sql;
	if(overlapping)
		sql = "SELECT id FROM shp_index WHERE maxX>=? AND minX<=? AND maxY>=? AND minY<=?;"; //Overlapping area
	else
		sql = "SELECT id FROM shp_index WHERE minX>=? AND maxX<=? AND minY>=? AND maxY<=?;"; //Within area

	sqlite3_stmt *pStmt =nullptr;
	int ret = sqlite3_prepare_v2(pDb, sql.c_str(), -1, &pStmt, nullptr);
	if(ret != SQLITE_OK)
	{
		cout << ret << " " << sqlite3_errmsg(pDb) << endl;
		return ret;
	}

	ret = sqlite3_bind_double(pStmt, 1, xMin);
	if(ret != SQLITE_OK) {cout << ret << " " << sqlite3_errmsg(pDb) << endl; return ret;}
	ret = sqlite3_bind_double(pStmt, 2, xMax);
	if(ret != SQLITE_OK) {cout << ret << " " << sqlite3_errmsg(pDb) << endl; return ret;}
	ret = sqlite3_bind_double(pStmt, 3, yMin);
	if(ret != SQLITE_OK) {cout << ret << " " << sqlite3_errmsg(pDb) << endl; return ret;}
	ret = sqlite3_bind_double(pStmt, 4, yMax);
	if(ret != SQLITE_OK) {cout << ret << " " << sqlite3_errmsg(pDb) << endl; return ret;}

	bool looping = true;
	while(looping)
	{
		ret = sqlite3_step(pStmt);

		if(ret == SQLITE_ROW)
		{
			int nCols = sqlite3_column_count(pStmt);
			if(nCols != 1)
			{
				cout << "Wrong number of columns returned" << endl;
				return -2;
			}

			int id = sqlite3_column_int(pStmt, 0);
			out.push_back(id);
		}
		else
			looping = false;
	}
	if(ret != SQLITE_DONE) {cout << "sqlite3_step " << ret << " " << sqlite3_errmsg(pDb) << endl; return ret;}

	ret = sqlite3_finalize(pStmt);
	if(ret != SQLITE_OK) {cout << "sqlite3_finalize " << ret << " " << sqlite3_errmsg(pDb) << endl;}
	return ret;
}

